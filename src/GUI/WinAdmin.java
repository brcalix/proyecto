
package GUI;

public class WinAdmin extends javax.swing.JFrame {
    
    WinMmeseros pWinMmeseros;
    WinMplatos pWinMplatos;
    WinRMesero pWinRMesero;
    WinRPlato pWinRPlato;
    WinPrin pWinPrin;
    
    public WinAdmin() {
        initComponents();
        pWinMmeseros = new WinMmeseros();
        pWinMplatos = new WinMplatos();
        pWinRMesero = new WinRMesero();
        pWinRPlato =new WinRPlato();
        pWinPrin = new WinPrin();
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnCerrarS = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        MmanMes = new javax.swing.JMenu();
        MmanPlat = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        MmejMes = new javax.swing.JMenu();
        MmejPlat = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnCerrarS.setText("Cerrar Sesión");
        btnCerrarS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarSActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrarS, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 310, -1, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fondo 2.jpg"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 510, 350));

        jMenu1.setText("Mantenimiento");
        jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu1MouseClicked(evt);
            }
        });

        MmanMes.setText("Meseros");
        MmanMes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                MmanMesMouseClicked(evt);
            }
        });
        MmanMes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MmanMesActionPerformed(evt);
            }
        });
        jMenu1.add(MmanMes);

        MmanPlat.setText("Platos");
        MmanPlat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                MmanPlatMouseClicked(evt);
            }
        });
        jMenu1.add(MmanPlat);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Reporte");

        MmejMes.setText("Mejor Mesero");
        MmejMes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                MmejMesMouseClicked(evt);
            }
        });
        jMenu2.add(MmejMes);

        MmejPlat.setText("Plato Destacado");
        MmejPlat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                MmejPlatMouseClicked(evt);
            }
        });
        jMenu2.add(MmejPlat);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCerrarSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarSActionPerformed
        pWinPrin.setVisible(true);
        this.setVisible(false);        
    }//GEN-LAST:event_btnCerrarSActionPerformed

    private void MmanMesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MmanMesActionPerformed
        
    }//GEN-LAST:event_MmanMesActionPerformed

    private void jMenu1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu1MouseClicked
        
    }//GEN-LAST:event_jMenu1MouseClicked

    private void MmanMesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MmanMesMouseClicked
        // TODO add your handling code here:
        pWinMmeseros.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_MmanMesMouseClicked

    private void MmanPlatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MmanPlatMouseClicked
        // TODO add your handling code here:
        pWinMplatos.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_MmanPlatMouseClicked

    private void MmejMesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MmejMesMouseClicked
        // TODO add your handling code here:
        pWinRMesero.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_MmejMesMouseClicked

    private void MmejPlatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MmejPlatMouseClicked
        // TODO add your handling code here:
        pWinRPlato.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_MmejPlatMouseClicked

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(WinAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(WinAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(WinAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(WinAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new WinAdmin().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu MmanMes;
    private javax.swing.JMenu MmanPlat;
    private javax.swing.JMenu MmejMes;
    private javax.swing.JMenu MmejPlat;
    private javax.swing.JButton btnCerrarS;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    // End of variables declaration//GEN-END:variables
}
